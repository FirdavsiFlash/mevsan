import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = () => ({
  modals: {
    status: true,
    modal_course: {
      status: false,
    },
    modal_free_lesson: {
      status: false,
    },
    modal_contacts: {
      status: false,
    },
    modal_groups: {
      status: false,
    },
    menubar: {
      status: false,
    },
    programms: {
      status: true,
    },
  },


})

const getters = {
  modals: state => state.modals,
}

const mutations = {
  OPEN_MODAL(state, details) {
    if (process.client && process.server) {

      if (state.modals.menubar.status) {
        document.querySelector('header .links').style.display = 'none'
      } else {
        document.querySelector('header .links').style.display = 'flex'
      }

    }
    if (state.modals[details.name].status) document.body.style.overflow = "scroll"
    else document.body.style.overflow = "hidden"
    // state.modals.forEach(element => {
    //   element[details.name].status = !element[details.name].status
    // });
    state.modals[details.name].status = !state.modals[details.name].status
    state.modals.status = !state.modals.status

    state.modals[details.name].text = details.text
    if (details.bg) state.modals.background.status = true

  },
}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}
