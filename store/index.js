import Vuex from 'vuex'
import Vue from 'vue'
import ui from './modules/ui.js'
import GetData from './modules/Data.js'


Vue.use(Vuex)

const state = () => ({
	
})

const getters = {
	ApiData: state => state.ApiData
}

const mutations = {
}

const actions = {
}
const modules = {
	ui,
	GetData
}
export default {
	state,
	getters,
	actions,
	mutations,
	modules
}
