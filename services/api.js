import axios from 'axios'
import config from '../nuxt.config.js'

export default () => {
	const options = {}

	options.baseURL = config.publicRuntimeConfig.baseURL
	options.contentType = 'application/json'

	const instance = axios.create(options)
	return instance
}
